package com.ag.flightscanner.configuration;

import com.ag.flightscanner.controllers.AirportController;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;


@Component
public class InnitializationComponent {

    @Autowired
    AirportController airportController;

    @Bean
    InitializingBean sendDatabase(){
        return() -> {
            Logger.getLogger(getClass()).info("Completed creating bean");
            airportController.requestLufthansaAirportsList();
        };
    }
}
