package com.ag.flightscanner.serializers;

import com.ag.flightscanner.model.journey.Flight;
import com.ag.flightscanner.model.journey.Schedule;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FlightDeserializer extends JsonDeserializer<List<Flight>> {

    private static final Logger LOG = Logger.getLogger(FlightDeserializer.class);

    @Override
    public List<Flight> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        List<Flight> result = new ArrayList<>();

        ObjectCodec codec = jsonParser.getCodec();
        TreeNode node = codec.readTree(jsonParser);
        Gson g = new Gson();

        if (node.isArray()) {
            Type listType = new TypeToken<List<Flight>>() {
            }.getType();
            result = g.fromJson(String.valueOf(node), listType);
        } else{
            result.add(g.fromJson(String.valueOf(node), Flight.class));
        }

        return result;
    }
}

