package com.ag.flightscanner.serializers;

import com.ag.flightscanner.model.journey.Airport;
import com.ag.flightscanner.services.AirportService;
import com.ag.flightscanner.services.AirportServiceImpl;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class AirportDeserializer extends JsonDeserializer<Airport> {

    private static final Logger LOG = Logger.getLogger(AirportDeserializer.class);

    @Autowired
    AirportService service;

    @Override
    public Airport deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        String airportCode = jsonParser.getText();
        Optional<Airport> airportOptional = service.getAirport(airportCode);
        if (airportOptional.isPresent()) {
            return airportOptional.get();
        }
        return null;
    }
}

