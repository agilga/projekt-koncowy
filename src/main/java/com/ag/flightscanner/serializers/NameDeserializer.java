package com.ag.flightscanner.serializers;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class NameDeserializer extends JsonDeserializer<Map> {

    private static final Logger LOG = Logger.getLogger(NameDeserializer.class);

    @Override
    public Map deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        HashMap<String, String> result = new HashMap<>();

        ObjectCodec codec = jsonParser.getCodec();
        TreeNode node = codec.readTree(jsonParser);
        TreeNode names = node.get("Name");

        if (names.isArray()) {
            for (JsonNode name : (ArrayNode) names) {
                String key = name.get("@LanguageCode").asText();
                String value = name.get("$").asText();
                result.put(key, value);
            }
        }
        return result;
    }
}

