package com.ag.flightscanner.serializers;

import com.ag.flightscanner.model.journey.Position;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.log4j.Logger;

import java.io.IOException;

public class PositionDeserializer extends JsonDeserializer<Position> {

    private static final Logger LOG = Logger.getLogger(PositionDeserializer.class);

    @Override
    public Position deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {

        JsonNode node = jsonParser.readValueAsTree();
        JsonNode positionJson = node.get("Coordinate");
        Double longitude = Double.valueOf(String.valueOf(positionJson.get("Longitude")));
        Double latitude = Double.valueOf(String.valueOf(positionJson.get("Latitude")));

        Position position = new Position();
        position.setLatitude(latitude);
        position.setLongitude(longitude);

        return position;
    }
}

