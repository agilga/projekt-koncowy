package com.ag.flightscanner.services;

import com.ag.flightscanner.model.journey.Schedule;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface ScheduleService {

    boolean addSchedule(Schedule scheduleToAdd);

    void addSchedules(List<Schedule> schedulesToAdd);

    ResponseEntity importFlights(String url, Map<String, String> vars);

    Iterable<Schedule> getAllSchedule();

    BigDecimal generatePrice();

    public Optional<Schedule> getSchedule(Long scheduleId);
}
