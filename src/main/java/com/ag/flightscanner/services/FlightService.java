package com.ag.flightscanner.services;


import com.ag.flightscanner.model.journey.Airport;
import com.ag.flightscanner.model.journey.Flight;

import java.time.LocalDateTime;
import java.util.List;

public interface FlightService {

    //chce dodać lot
    boolean addFlight(Flight flightToAdd);

    //chce dodac liste lotow
    void addFlights (List<Flight> flightsToAdd);

    //chce pobrac liste lotów
    Iterable<Flight> getAllFlights();

    //chce pobrac liste lotow z.. do..;
    Iterable<Flight> getAllFlights(Airport origin,
                                   Airport destination);

    //chce pobrac liste lotow od.. do..;
    Iterable<Flight> getAllFlights(LocalDateTime origin,
                                   LocalDateTime destination);

    //chce pobrac liste lotow z.. do.. od.. do..;
    Iterable<Flight> getAllFlights(Airport originPlace,
                                   Airport destinationPlace,
                                   LocalDateTime originDateTime,
                                   LocalDateTime destinationLocalDateTime);
    //-chce importowac loty
    Iterable<Flight> importFlights();



}
