package com.ag.flightscanner.services;

import com.ag.flightscanner.dao.UserDaoImpl;
import com.ag.flightscanner.model.journey.Schedule;
import com.ag.flightscanner.model.users.PurchasedJourney;
import com.ag.flightscanner.model.users.User;
import com.ag.flightscanner.model.users.UserData;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
//import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;
import java.util.Optional;


@Service(value = "userService")
@Transactional
public class UserServiceImpl implements UserService {

    private static final Logger LOG = Logger.getLogger(UserServiceImpl.class);
    @Autowired
    UserDaoImpl userDao;

    @Override
    public boolean userExists(String login) {
        LOG.debug("Checking if user: " + login + "exist.");
        return userDao.existsByLogin(login);
    }

    @Override
    public boolean registerUser(User userToRegister, UserData data) {
        LOG.debug("Registering user: " + userToRegister + " with data: " + data + ".");
        userDao.registerUser(userToRegister, data);
        return true;
    }

    @Override
    public Iterable<User> getAllUsers() {
        LOG.debug("Getting all users.");
        return userDao.findAll();
    }

    @Override
    public void updateUser(User user) {
        LOG.debug("Updating user: " + user.getId() + ".");
        userDao.update(user);
    }

    @Override
    public void deleteUser(User user) {
        LOG.debug("Deleting user: " + user.getId() + ".");
        userDao.delete(user);
    }


    @Override
    public Optional<User> getUserById(Long id) {
        LOG.debug("Getting user by id : " + id + ".");
        return userDao.findById(id);
    }

    @Override
    public Optional<User> getUserByLogin(String login) {
        LOG.debug("Getting user by login : " + login + ".");
        return userDao.findByLogin(login);
    }

    @Override
    public boolean confirmPassword(User user, String password) {
        return user.getPasswordHash().equals(DigestUtils.md5Hex(password));
    }


    @Override
    public void addScheduleToSaved(Long userId, Schedule schedule) {
        LOG.debug("Adding schedule : " + schedule + " to saved.");
        Optional.ofNullable(getUserById(userId)).ifPresent(user -> {
            User userToUpdate = user.get();
            userToUpdate.getSavedFlights().add(schedule);
            updateUser(userToUpdate);
        });
    }

    @Override
    public void deleteScheduleFromSaved(Long userId, Schedule schedule) {
        LOG.debug("Deleting schedule : " + schedule + " from saved.");
        Optional.ofNullable(getUserById(userId)).ifPresent(user -> {
            User userToUpdate = user.get();
            List<Schedule> schedules= userToUpdate.getSavedFlights();
            Schedule scheduleToRemove = null;
            for (Schedule item : schedules) {
                if(schedule.getId()==item.getId()){
                    scheduleToRemove=item;
                }
            }
            schedules.remove(scheduleToRemove);
            userToUpdate.setSavedFlights(schedules);
            updateUser(userToUpdate);
        });
    }

    @Override
    public void addScheduleToBought(Long userId, Schedule schedule) {
        LOG.debug("Adding schedule : " + schedule + " to user flights.");
        Optional.ofNullable(getUserById(userId)).ifPresent(user -> {
            User userToUpdate = user.get();
            userToUpdate.getPurchasedFlights().add(new PurchasedJourney(schedule, false));
            updateUser(userToUpdate);
        });
    }


    @Override
    public void saveUserData(UserData userData) {
        LOG.debug("Adding userdata : " + userData + ".");
        userDao.save(userData);
    }

}
