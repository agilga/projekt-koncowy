package com.ag.flightscanner.services;

import com.ag.flightscanner.dao.AirportDaoImpl;
import com.ag.flightscanner.model.journey.Airport;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;


@Service(value = "airportService")
@Transactional
public class AirportServiceImpl implements AirportService {


    private static final Logger LOG = Logger.getLogger(AirportServiceImpl.class);
    static final String LH_ACCESS_TOKEN = "7nwxheptvnfu8kqy5sbq8wke";


    @Autowired
    AirportDaoImpl airportDao;

    @Override
    public boolean airportExists(String code) {
        LOG.debug("Checking if airport: " + code + "exist.");
        return airportDao.existsByCode(code);
    }

    @Override
    public boolean addAirport(Airport airportToAdd) {
        LOG.debug("Adding airport");
        if(!airportExists(airportToAdd.getAirportCode())) {
            airportDao.save(airportToAdd);
            return true;
        }
        return false;
    }

    @Override
    public void addAirports(List<Airport> airportsToAdd) {
        LOG.debug("Adding airports");
        airportsToAdd.forEach(airport -> addAirport(airport));
    }

    @Override
    public ResponseEntity importAirports(String url) {
        LOG.debug("Importing airports from Lufthansa Api");
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + LH_ACCESS_TOKEN);
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, Object.class);
        return responseEntity;
    }

    @Override
    public Iterable<Airport> getAllAirports() {
        LOG.debug("Getting all airports.");
        return airportDao.findAll();
    }

    @Override
    public Optional<Airport> getAirport(String airportCode) {
        LOG.debug("Getting airport with airportCode: " + airportCode +".");
        return airportDao.findByAirportCode(airportCode);
    }


}
