package com.ag.flightscanner.services;


import com.ag.flightscanner.model.journey.Schedule;
import com.ag.flightscanner.model.users.User;
import com.ag.flightscanner.model.users.UserData;

import java.util.Optional;

public interface UserService {

    //user story - chce sprawdzic czy dany uczytkownik (o danym loginie) juz istnieje wiec potrzebuje metody do sprawdzania tego
    boolean userExists(String login);

    //user story - chce zarejestrowac uzytkownika
    boolean registerUser(User userToRegister, UserData data);

        //user story - chce wylistowac wszystkich uzytkownikow
    Iterable<User> getAllUsers();

    //user story - chce edytowac uzytkownika
    void updateUser(User user);

    //user story - chce usunac uzytkownika
    void deleteUser(User user);

    Optional<User> getUserById(Long id);

    void saveUserData(UserData userData);

    Optional<User> getUserByLogin(String login);

    boolean confirmPassword(User user, String password);

    void addScheduleToSaved(Long userId, Schedule schedule);

    void deleteScheduleFromSaved(Long userId, Schedule schedule);

    void addScheduleToBought(Long userId, Schedule schedule);
}
