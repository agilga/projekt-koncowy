package com.ag.flightscanner.services;

import com.ag.flightscanner.dao.FlightDaoImpl;
import com.ag.flightscanner.model.journey.Airport;
import com.ag.flightscanner.model.journey.Flight;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
//import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.time.LocalDateTime;
import java.util.List;


@Service(value = "flightService")
@Transactional
public class FlightServiceImpl implements FlightService {

    private static final Logger LOG = Logger.getLogger(FlightServiceImpl.class);
    @Autowired
    FlightDaoImpl flightDao;

    @Override
    public boolean addFlight(Flight flightToAdd) {
//        LOG.debug("Adding flight: from " + flightToAdd.getOriginPlace() + " to " + flightToAdd.getDestinationPlace() + ".");
        flightDao.save(flightToAdd);
        return true;
    }

    @Override
    public void addFlights(List<Flight> flightsToAdd) {
        LOG.debug("Adding flights");
        flightsToAdd.forEach(flight -> addFlight(flight));
    }

    @Override
    public Iterable<Flight> getAllFlights() {
        LOG.debug("Getting all flights.");
        return flightDao.findAll();
    }

    @Override
    public Iterable<Flight> getAllFlights(Airport origin,
                                          Airport destination) {


        return flightDao.findAllByOriginPlaceAndDestinationPlace(origin, destination);
    }

    @Override
    public Iterable<Flight> getAllFlights(Airport originPlace,
                                          Airport destinationPlace,
                                          LocalDateTime originDateTime,
                                          LocalDateTime destinationLocalDateTime) {
//        LOG.debug("Getting all flights from: "
//                + originPlace.getName() + " (" + originDateTime + ")" + " to: "
//                + destinationPlace.getName() + " (" + originDateTime + ")");

        return flightDao.findAllByOriginPlaceAndDestinationPlaceAAndOriginDateTimeAndDestinationDateTime(
                originPlace,
                destinationPlace,
                originDateTime,
                destinationLocalDateTime);
    }

    @Override
    public Iterable<Flight> getAllFlights(LocalDateTime origin,
                                          LocalDateTime destination) {

        LOG.debug("Getting all flights from: " + origin + " to: " + destination);
        return flightDao.findAllByOriginDateTimeAndDestinationDateTime(origin, destination);
    }


    @Override
    public List<Flight> importFlights() {
        LOG.debug("Importing all flights from Lufthansa Api" );
        // TODO: do zrobienia importowanie lotów od danych przwoznikow
                throw new UnsupportedOperationException();
    }
}
