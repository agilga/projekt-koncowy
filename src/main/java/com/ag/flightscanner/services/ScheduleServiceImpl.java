package com.ag.flightscanner.services;

import com.ag.flightscanner.dao.AbstractDao;
import com.ag.flightscanner.dao.ScheduleDaoImpl;
import com.ag.flightscanner.model.journey.Flight;
import com.ag.flightscanner.model.journey.Schedule;
import com.ag.flightscanner.model.users.User;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.*;


@Service(value = "scheduleService")
@Transactional
public class ScheduleServiceImpl implements ScheduleService {

    private static final Logger LOG = Logger.getLogger(ScheduleServiceImpl.class);

    @Autowired
    ScheduleDaoImpl scheduleDao;

    @Override
    public boolean addSchedule(Schedule scheduleToAdd) {
//        LOG.debug("Adding schedule: from " + flightToAdd.getOriginPlace() + " to " + flightToAdd.getDestinationPlace() + ".");
        scheduleToAdd.getTotalJourney().setPrice(generatePrice());
        scheduleDao.save(scheduleToAdd);
        return true;
    }

    @Override
    public void addSchedules(List<Schedule> schedulesToAdd) {
        LOG.debug("Adding schedules");
        schedulesToAdd.forEach(schedule -> addSchedule(schedule));
    }

    @Override
    public ResponseEntity importFlights(String url, Map<String, String> vars) {
        LOG.debug("Importing schedules from Lufthansa Api");
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + AirportServiceImpl.LH_ACCESS_TOKEN);
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, Object.class, vars);
        return responseEntity;
    }

    @Override
    public Iterable<Schedule> getAllSchedule() {
        LOG.debug("Getting all schedules");
        return scheduleDao.findAll();
    }

    @Override
    public BigDecimal generatePrice() {
        LOG.debug("Getting schedule price");
        int randomNumber = (new Random().nextInt(50)+1)*100;
        return new BigDecimal(randomNumber).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    @Override
    public Optional<Schedule> getSchedule(Long scheduleId) {
        LOG.debug("Getting schedule with id: " + scheduleId);
        return scheduleDao.findById(scheduleId);
    }


}
