package com.ag.flightscanner.services;

import com.ag.flightscanner.model.journey.Airport;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface AirportService {

    boolean airportExists(String code);

    boolean addAirport(Airport airportToAdd);

    void addAirports(List<Airport> airportsToAdd);

    ResponseEntity importAirports(String url);

    Iterable<Airport> getAllAirports();

    Optional<Airport> getAirport(String airportCode);
}
