package com.ag.flightscanner.model.users;

import com.ag.flightscanner.model.journey.Schedule;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name="app_user")
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String login;

    private String passwordHash;

    @OneToOne(cascade = CascadeType.ALL)
    private UserData userData;

    @ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @Fetch(FetchMode.SUBSELECT)
    @JoinTable(name = "user_flights",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "journeyBought_id")})
    private List<PurchasedJourney> purchasedFlights;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Fetch(FetchMode.SUBSELECT)
    @JoinTable(name = "user_savedFlights",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "schedule_id")})
    private List<Schedule> savedFlights;

    public User() {
    }

    public User(String login, String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public User(String login, String passwordHash, UserData data) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.userData = data;
    }

    public User(String login, String passwordHash, String firstName, String lastName) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.userData.setFirstName(firstName);
        this.userData.setLastName(lastName);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public List<PurchasedJourney> getPurchasedFlights() {
        return purchasedFlights;
    }

    public void setPurchasedFlights(List<PurchasedJourney> purchasedFlights) {
        this.purchasedFlights = purchasedFlights;
    }

    public List<Schedule> getSavedFlights() {
        return savedFlights;
    }

    public void setSavedFlights(List<Schedule> savedFlights) {
        this.savedFlights = savedFlights;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", data=" + userData +
                '}';
    }

    public static class UserBuilder{

        private String login;
        private String passwordHash;
        private UserData data;
        private String firstName;
        private String lastName;
        private UserData userData;
        private List<Schedule> flights;
        private List<Schedule> savedFlights;

        public UserBuilder setLogin(String login) {
            this.login = login;
            return this;
        }

        public UserBuilder setPasswordHash(String passwordHash) {
            this.passwordHash = passwordHash;
            return this;
        }

        public UserBuilder setData(UserData data) {
            this.data = data;
            return this;
        }

        public UserBuilder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public UserBuilder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public UserBuilder setUserData(UserData userData) {
            this.userData = userData;
            return this;
        }

        public UserBuilder setFlights(List<Schedule> flights) {
            this.flights = flights;
            return this;
        }

        public UserBuilder setSavedFlights(List<Schedule> savedFlights) {
            this.savedFlights = savedFlights;
            return this;
        }

        public User createUser() {
            return new User(login, passwordHash);
        }
    }
}
