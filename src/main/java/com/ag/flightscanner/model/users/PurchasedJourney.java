package com.ag.flightscanner.model.users;

import com.ag.flightscanner.model.journey.Schedule;

import javax.persistence.*;

@Entity
public class PurchasedJourney {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToOne
    private Schedule schedule;

    private boolean paymentStatus;

    public PurchasedJourney() {
    }

    public PurchasedJourney(Schedule schedule, boolean paymentStatus) {
        this.schedule = schedule;
        this.paymentStatus = paymentStatus;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public boolean isPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(boolean paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
}
