package com.ag.flightscanner.model.users;

public class AuthenticationPassword {
    private Long userId;
    private String newPassword;
    private String passwordConfirm;
    private String oldPassword;

    public AuthenticationPassword() {
    }

    public AuthenticationPassword(Long userId, String newPassword, String oldPassword) {
        this.userId = userId;
        this.newPassword = newPassword;
        this.oldPassword = oldPassword;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }
}


