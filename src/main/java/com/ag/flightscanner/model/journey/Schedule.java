package com.ag.flightscanner.model.journey;

import com.ag.flightscanner.serializers.FlightDeserializer;
import com.ag.flightscanner.serializers.NameDeserializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.persistence.*;
import java.util.List;

//@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class Schedule {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonProperty(value = "TotalJourney", required = false)
    private TotalJourney TotalJourney;
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonProperty(value = "Flight", required = false)
    @JsonDeserialize(using = FlightDeserializer.class)
    private List<Flight> Flight;


    public Schedule() {
    }

    public Schedule(TotalJourney totalJourney, List<Flight> flights) {
        this.TotalJourney = totalJourney;
        this.Flight = flights;
    }

    public TotalJourney getTotalJourney() {
        return TotalJourney;
    }

    public void setTotalJourney(TotalJourney totalJourney) {
        this.TotalJourney = totalJourney;
    }

    public List<Flight> getFlights() {
        return Flight;
    }

    public void setFlights(List<Flight> flights) {
        this.Flight = flights;
    }

    public long getId() {
        return id;
    }

    public List<com.ag.flightscanner.model.journey.Flight> getFlight() {
        return Flight;
    }

    public void setFlight(List<com.ag.flightscanner.model.journey.Flight> flight) {
        Flight = flight;
    }

}
