package com.ag.flightscanner.model.journey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
//@JsonIgnoreProperties (ignoreUnknown = true)
public class Details {
    @Id
    @GeneratedValue
    private long id;

    @JsonProperty(value = "Stops", required = false)
    @ManyToOne(cascade = CascadeType.ALL)
    private Stops Stops;

    @JsonProperty(value = "DaysOfOperation", required = false)
    private int DaysOfOperation;

    @OneToOne(cascade = CascadeType.ALL)
    @JsonProperty(value = "DatePeriod", required = false)
    private DatePeriod DatePeriod;


    public Details() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public com.ag.flightscanner.model.journey.Stops getStops() {
        return Stops;
    }

    public void setStops(com.ag.flightscanner.model.journey.Stops stops) {
        Stops = stops;
    }

    public int getDaysOfOperation() {
        return DaysOfOperation;
    }

    public void setDaysOfOperation(int daysOfOperation) {
        DaysOfOperation = daysOfOperation;
    }

    public com.ag.flightscanner.model.journey.DatePeriod getDatePeriod() {
        return DatePeriod;
    }

    public void setDatePeriod(com.ag.flightscanner.model.journey.DatePeriod datePeriod) {
        DatePeriod = datePeriod;
    }
}