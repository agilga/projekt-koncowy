package com.ag.flightscanner.model.journey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
//@JsonIgnoreProperties(ignoreUnknown = true)
public class Compartment {
    @Id
    @GeneratedValue
    private long id;
    @JsonProperty(value = "ClassCode", required = false)
    private String ClassCode;
    @JsonProperty(value = "ClassDesc", required = false)
    private String ClassDesc;
    @JsonProperty(value = "FlyNet", required = false)
    private boolean FlyNet;
    @JsonProperty(value = "SeatPower", required = false)
    private boolean SeatPower;
    @JsonProperty(value = "Usb", required = false)
    private boolean Usb;
    @JsonProperty(value = "LiveTv", required = false)
    private boolean LiveTv;

    public Compartment() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getClassCode() {
        return ClassCode;
    }

    public void setClassCode(String classCode) {
        ClassCode = classCode;
    }

    public String getClassDesc() {
        return ClassDesc;
    }

    public void setClassDesc(String classDesc) {
        ClassDesc = classDesc;
    }

    public boolean isFlyNet() {
        return FlyNet;
    }

    public void setFlyNet(boolean flyNet) {
        FlyNet = flyNet;
    }

    public boolean isSeatPower() {
        return SeatPower;
    }

    public void setSeatPower(boolean seatPower) {
        SeatPower = seatPower;
    }

    public boolean isUsb() {
        return Usb;
    }

    public void setUsb(boolean usb) {
        Usb = usb;
    }

    public boolean isLiveTv() {
        return LiveTv;
    }

    public void setLiveTv(boolean liveTv) {
        LiveTv = liveTv;
    }
}
