package com.ag.flightscanner.model.journey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.List;

@Entity
//@JsonIgnoreProperties(ignoreUnknown = true)
public class OnBoardEquipment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @JsonProperty(value = "InflightEntertainment", required = false)
    private boolean InflightEntertainment;
    @OneToMany(cascade = CascadeType.PERSIST)
    @JsonProperty(value = "Compartment", required = false)
    private List<Compartment> Compartment;

    public OnBoardEquipment() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isInflightEntertainment() {
        return InflightEntertainment;
    }

    public void setInflightEntertainment(boolean inflightEntertainment) {
        InflightEntertainment = inflightEntertainment;
    }

    public List<Compartment> getCompartment() {
        return Compartment;
    }

    public void setCompartment(List<Compartment> compartments) {
        Compartment = compartments;
    }

}
