package com.ag.flightscanner.model.journey;

import com.ag.flightscanner.serializers.NameDeserializer;
import com.ag.flightscanner.serializers.PositionDeserializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.persistence.*;
import java.util.HashMap;

@Entity
@Table(name="airport")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Airport {
    @Id
    @GeneratedValue
    private long id;
    @JsonProperty(value = "AirportCode", required = false)
    private String AirportCode;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JsonProperty(value = "Position", required = false)
    @JsonDeserialize(using = PositionDeserializer.class)
    private Position Position;
    @JsonProperty(value = "CityCode", required = false)
    private String CityCode;
    @JsonProperty(value = "CountryCode", required = false)
    private String CountryCode;
    @JsonProperty(value = "Names", required = false)
    @JsonDeserialize(using = NameDeserializer.class)
    private HashMap<String, String> Names;
    @JsonProperty(value = "UtcOffset", required = false)
    private int UtcOffset;
    @JsonProperty(value = "TimeZoneId", required = false)
    private String TimeZoneId;

    public Airport() {
    }

    public Airport(String airportCode, com.ag.flightscanner.model.journey.Position position, String cityCode, String countryCode, HashMap<String, String> names, int utcOffset, String timeZoneId) {
        AirportCode = airportCode;
        Position = position;
        CityCode = cityCode;
        CountryCode = countryCode;
        Names = names;
        UtcOffset = utcOffset;
        TimeZoneId = timeZoneId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAirportCode() {
        return AirportCode;
    }

    public void setAirportCode(String airportCode) {
        AirportCode = airportCode;
    }

    public com.ag.flightscanner.model.journey.Position getPosition() {
        return Position;
    }

    public void setPosition(com.ag.flightscanner.model.journey.Position position) {
        Position = position;
    }

    public String getCityCode() {
        return CityCode;
    }

    public void setCityCode(String cityCode) {
        CityCode = cityCode;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public HashMap<String, String> getNames() {
        return Names;
    }

    public void setNames(HashMap<String, String> names) {
        Names = names;
    }

    public int getUtcOffset() {
        return UtcOffset;
    }

    public void setUtcOffset(int utcOffset) {
        UtcOffset = utcOffset;
    }

    public String getTimeZoneId() {
        return TimeZoneId;
    }

    public void setTimeZoneId(String timeZoneId) {
        TimeZoneId = timeZoneId;
    }
}
