package com.ag.flightscanner.model.journey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "flight")
//@JsonIgnoreProperties(ignoreUnknown = true)
public class Flight {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long Id;

    @OneToOne(cascade = CascadeType.ALL)
    @JsonProperty(value = "Departure", required = false)
    private Departure Departure;

    @OneToOne(cascade = CascadeType.ALL)
    @JsonProperty(value = "Arrival", required = false)
    private Arrival Arrival;

    @ManyToOne(cascade = CascadeType.ALL)
    @JsonProperty(value = "MarketingCarrier", required = false)
    private Carrier MarketingCarrier;

    @ManyToOne(cascade = CascadeType.ALL)
    @JsonProperty(value = "OperatingCarrier", required = false)
    private Carrier OperatingCarrier;

    @ManyToOne(cascade = CascadeType.ALL)
    @JsonProperty(value = "Equipment", required = false)
    private Equipment Equipment;

    @OneToOne(cascade = CascadeType.ALL)
    @JsonProperty(value = "Details", required = false)
    private Details Details;

    public Flight() {
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public com.ag.flightscanner.model.journey.Departure getDeparture() {
        return Departure;
    }

    public void setDeparture(com.ag.flightscanner.model.journey.Departure departure) {
        Departure = departure;
    }

    public com.ag.flightscanner.model.journey.Arrival getArrival() {
        return Arrival;
    }

    public void setArrival(com.ag.flightscanner.model.journey.Arrival arrival) {
        Arrival = arrival;
    }

    public Carrier getMarketingCarrier() {
        return MarketingCarrier;
    }

    public void setMarketingCarrier(Carrier marketingCarrier) {
        MarketingCarrier = marketingCarrier;
    }

    public Carrier getOperatingCarrier() {
        return OperatingCarrier;
    }

    public void setOperatingCarrier(Carrier operatingCarrier) {
        OperatingCarrier = operatingCarrier;
    }

    public com.ag.flightscanner.model.journey.Equipment getEquipment() {
        return Equipment;
    }

    public void setEquipment(com.ag.flightscanner.model.journey.Equipment equipment) {
        Equipment = equipment;
    }

    public com.ag.flightscanner.model.journey.Details getDetails() {
        return Details;
    }

    public void setDetails(com.ag.flightscanner.model.journey.Details details) {
        Details = details;
    }

}
