package com.ag.flightscanner.model.journey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class Stops {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @JsonProperty(value = "StopQuantityl", required = false)
    private int StopQuantityl;

    public Stops() {
    }

    public int getStopQuantityl() {
        return StopQuantityl;
    }

    public void setStopQuantityl(int stopQuantityl) {
        StopQuantityl = stopQuantityl;
    }
}
