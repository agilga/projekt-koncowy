package com.ag.flightscanner.model.journey;

import com.ag.flightscanner.serializers.AirportDeserializer;
import com.ag.flightscanner.serializers.FlightDeserializer;
import com.ag.flightscanner.serializers.ScheduleTimeDeserializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Departure {

    @Id
    @GeneratedValue
    private long id;
//    @ManyToOne(cascade = CascadeType.ALL)
    @JsonProperty(value = "AirportCode", required = false)
//    @JsonDeserialize(using = AirportDeserializer.class)
    private String AirportCode;

//        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-ddTHH:mm")
    @JsonProperty(value = "ScheduledTimeLocal", required = false)
    @ManyToOne(cascade = CascadeType.ALL)
    private ScheduledTime ScheduledTimeLocal;

    @JsonProperty(value = "Terminal", required = false)
    @ManyToOne(cascade = CascadeType.ALL)
    private Terminal Terminal;

    public Departure() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAirportCode() {
        return AirportCode;
    }

    public void setAirportCode(String airportCode) {
        AirportCode = airportCode;
    }

    public ScheduledTime getScheduledTimeLocal() {
        return ScheduledTimeLocal;
    }

    public void setScheduledTimeLocal(ScheduledTime scheduledTimeLocal) {
        ScheduledTimeLocal = scheduledTimeLocal;
    }

    public com.ag.flightscanner.model.journey.Terminal getTerminal() {
        return Terminal;
    }

    public void setTerminal(com.ag.flightscanner.model.journey.Terminal terminal) {
        Terminal = terminal;
    }
}
