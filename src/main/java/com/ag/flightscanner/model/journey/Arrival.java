package com.ag.flightscanner.model.journey;

import com.ag.flightscanner.serializers.ScheduleTimeDeserializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
//@JsonIgnoreProperties(ignoreUnknown = true)
public class Arrival {
    @Id
    @GeneratedValue
    private long id;

    @JsonProperty(value = "AirportCode", required = false)
    private String AirportCode;

    @JsonDeserialize(using = ScheduleTimeDeserializer.class)
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-ddTHH:mm")

    @JsonProperty(value = "ScheduledTimeLocal", required = false)
    @ManyToOne(cascade = CascadeType.ALL)
    private ScheduledTime ScheduledTimeLocal;

    @JsonProperty(value = "Terminal", required = false)
    @ManyToOne(cascade = CascadeType.ALL)
    private Terminal Terminal;

    public Arrival() {
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAirportCode() {
        return AirportCode;
    }

    public void setAirportCode(String airportCode) {
        AirportCode = airportCode;
    }

    public ScheduledTime getScheduledTimeLocal() {
        return ScheduledTimeLocal;
    }

    public void setScheduledTimeLocal(ScheduledTime scheduledTimeLocal) {
        ScheduledTimeLocal = scheduledTimeLocal;
    }

    public com.ag.flightscanner.model.journey.Terminal getTerminal() {
        return Terminal;
    }

    public void setTerminal(com.ag.flightscanner.model.journey.Terminal terminal) {
        Terminal = terminal;
    }
}
