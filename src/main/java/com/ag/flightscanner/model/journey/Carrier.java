package com.ag.flightscanner.model.journey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
//@JsonIgnoreProperties(ignoreUnknown = true)
public class Carrier {
    @Id
    @GeneratedValue
    private Long id;
    @JsonProperty(value = "AirlineID", required = false)
    private String AirlineID;
    @JsonProperty(value = "FlightNumber", required = false)
    private int FlightNumber;

    public Carrier() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAirlineID() {
        return AirlineID;
    }

    public void setAirlineID(String airlineID) {
        AirlineID = airlineID;
    }

    public int getFlightNumber() {
        return FlightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        FlightNumber = flightNumber;
    }
}
