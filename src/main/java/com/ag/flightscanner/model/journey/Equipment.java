package com.ag.flightscanner.model.journey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
//@JsonIgnoreProperties(ignoreUnknown = true)
public class Equipment {

    @Id
    @GeneratedValue
    private long id;
    @JsonProperty(value = "AircraftCode", required = false)
    private String AircraftCode;
    @ManyToOne(cascade = CascadeType.ALL)
    @JsonProperty(value = "OnBoardEquipment", required = false)
    private OnBoardEquipment OnBoardEquipment;

    public Equipment() {
    }


    public String getAircraftCode() {
        return AircraftCode;
    }

    public void setAircraftCode(String aircraftCode) {
        AircraftCode = aircraftCode;
    }

    public com.ag.flightscanner.model.journey.OnBoardEquipment getOnBoardEquipment() {
        return OnBoardEquipment;
    }

    public void setOnBoardEquipment(com.ag.flightscanner.model.journey.OnBoardEquipment onBoardEquipment) {
        OnBoardEquipment = onBoardEquipment;
    }
}
