package com.ag.flightscanner.model.journey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

//@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class TotalJourney {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @JsonProperty(value = "Duration", required = false)
    private String Duration;

    @JsonProperty(required = false)
    private BigDecimal price;

    public TotalJourney() {
    }

    public TotalJourney(String duration) {
        this.Duration = duration;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        this.Duration = duration;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
