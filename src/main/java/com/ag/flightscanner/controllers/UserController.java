package com.ag.flightscanner.controllers;

import com.ag.flightscanner.model.journey.Schedule;
import com.ag.flightscanner.model.users.AuthenticationPassword;
import com.ag.flightscanner.model.users.User;
import com.ag.flightscanner.model.users.UserData;
import com.ag.flightscanner.model.responses.Response;
import com.ag.flightscanner.model.responses.ResponseFactory;
import com.ag.flightscanner.services.ScheduleService;
import com.ag.flightscanner.services.UserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

@RestController
@RequestMapping(value = "/rest")
public class UserController {

    @Autowired
    UserService service;
    @Autowired
    ScheduleService scheduleService;


    @RequestMapping(value = "/registerUserWithData", method = RequestMethod.POST)
    public ResponseEntity<Response> registerUserWithData(@RequestBody User user) {
        Logger.getLogger(getClass()).debug("Requested registerUser with user:" + user);

        if (service.userExists(user.getLogin())) {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User exists"),
                    HttpStatus.BAD_REQUEST);
        } else {
            String password = user.getPasswordHash();
            user.setPasswordHash(DigestUtils.md5Hex(password));

            service.registerUser(user, user.getUserData());
            return new ResponseEntity<Response>(
                    ResponseFactory.success(user.getId()),
                    HttpStatus.OK);
        }
    }


    @RequestMapping(value = "/getUsers", method = RequestMethod.GET)
    public ResponseEntity<Iterable<User>> requestUserList() {
        return new ResponseEntity<Iterable<User>>(service.getAllUsers(), HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
    public ResponseEntity<Response> getUserWithId(@PathVariable Long userId) {
        Optional<User> user = service.getUserById(userId);
        if (user.isPresent()) {
            return new ResponseEntity<Response>(
                    ResponseFactory.success(user.get()), HttpStatus.OK);
        } else {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User with that id does not exist."), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/userExists/{login}", method = RequestMethod.GET)
    public ResponseEntity<Response> requestUserExists(@PathVariable String login) {
        return new ResponseEntity<Response>(
                ResponseFactory.success(service.userExists(login)), HttpStatus.OK);

    }

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.PUT)
    public ResponseEntity<Response> addDataToUser(@PathVariable Long userId,
                                                  @RequestBody UserData userData) {

        Optional<User> userOptional = service.getUserById(userId);
        if (!userOptional.isPresent()) {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User with that id does not exist."), HttpStatus.BAD_REQUEST);
        }
        service.saveUserData(userData);
        User user = userOptional.get();
        user.setUserData(userData);
        service.updateUser(user);

        return new ResponseEntity<Response>(
                ResponseFactory.success(user), HttpStatus.OK);
    }

    @RequestMapping(value = "/userprofile/{userId}/updateUser", method = RequestMethod.PUT)
    public ResponseEntity<Response> requestUpdateUser(@PathVariable Long userId,
                                                      @RequestBody UserData userData) {
        Optional<User> userOptional = service.getUserById(userId);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            Optional.ofNullable(userData.getFirstName())
                    .ifPresent(firstName -> user.getUserData().setFirstName(firstName));
            Optional.ofNullable(userData.getLastName())
                    .ifPresent(lastName -> user.getUserData().setLastName(lastName));
            Optional.ofNullable(userData.getEmail())
                    .ifPresent(email -> user.getUserData().setEmail(email));
            Optional.ofNullable(userData.getAddress().getStreet())
                    .ifPresent(street -> user.getUserData().getAddress().setStreet(street));
            Optional.ofNullable(userData.getAddress().getHouseNumber())
                    .ifPresent(houseNumber -> user.getUserData().getAddress().setHouseNumber(houseNumber));
            Optional.ofNullable(userData.getAddress().getCity())
                    .ifPresent(city -> user.getUserData().getAddress().setCity(city));
            Optional.ofNullable(userData.getAddress().getZipCode())
                    .ifPresent(zipCode -> user.getUserData().getAddress().setZipCode(zipCode));
            Optional.ofNullable(userData.getAddress().getCountry())
                    .ifPresent(country -> user.getUserData().getAddress().setCountry(country));

            service.updateUser(user);
            return new ResponseEntity<Response>(
                    ResponseFactory.success(user.getId()),
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("Update failed"),
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/user/{userId}/save-schedule/{scheduleId}", method = RequestMethod.PUT)
    public void trySaveSchedule(@PathVariable Long userId,
                                @PathVariable Long scheduleId) {
        scheduleService.getSchedule(scheduleId)
                .ifPresent(schedule ->service.addScheduleToSaved(userId, schedule));
    }

    @RequestMapping(value = "/user/{userId}/usubscribe-schedule/{scheduleId}", method = RequestMethod.PUT)
    public void tryUnsubsrcibeSchedule(@PathVariable Long userId,
                                @PathVariable Long scheduleId) {
        scheduleService.getSchedule(scheduleId)
                .ifPresent(schedule ->service.deleteScheduleFromSaved(userId, schedule));
    }

    @RequestMapping(value = "/user/{userId}/buy-schedule/{scheduleId}", method = RequestMethod.PUT)
    public void tryBuySchedule(@PathVariable Long scheduleId,
                                @PathVariable Long userId) {
        scheduleService.getSchedule(scheduleId)
                .ifPresent(schedule ->service.addScheduleToBought(userId, schedule));
    }

}

