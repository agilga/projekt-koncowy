package com.ag.flightscanner.controllers;

import com.ag.flightscanner.model.journey.Aircraft;
import com.ag.flightscanner.model.journey.Flight;
import com.ag.flightscanner.model.journey.Position;
import com.ag.flightscanner.model.journey.Schedule;
import com.ag.flightscanner.model.responses.Response;
import com.ag.flightscanner.model.responses.ResponseFactory;
import com.ag.flightscanner.services.FlightService;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


import java.util.*;

@RestController
public class FlightController {

    private static final Logger LOG = Logger.getLogger(FlightController.class);

    @Autowired
    FlightService service;

    @RequestMapping(value = "/addFlight", method = RequestMethod.POST)
    public ResponseEntity<Flight> requestAddFlight(@RequestBody Flight flight) {
        Logger.getLogger(getClass()).debug("Request addFlight");

        Boolean result = service.addFlight(flight);

        if (!result) {
            return new ResponseEntity<Flight>(flight, HttpStatus.BAD_REQUEST);

        } else {
            return new ResponseEntity<Flight>(flight, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/getFlights", method = RequestMethod.GET)
    public ResponseEntity<List<Flight>> requestFlightList() {
        return new ResponseEntity<List<Flight>>((List<Flight>) service.getAllFlights(), HttpStatus.OK);
    }

    @RequestMapping(value = "/aircrafts", method = RequestMethod.GET)
    public ResponseEntity requestAllAircraftsPositions() {

        String url = "https://opensky-network.org/api/states/all";

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, Object.class);
        HttpStatus statusCode = responseEntity.getStatusCode();
        if (statusCode.value() != 200) {
            return responseEntity;
        }
        Object objects = responseEntity.getBody();
        JSONObject object = new JSONObject((Map) objects);
        JSONArray aircrafts = new JSONArray();
        aircrafts.addAll((ArrayList) (object.get("states")));
        List<Aircraft> aircraftsList = new ArrayList<>();

        for (Object o : aircrafts) {
            Aircraft aircraft = new Aircraft();
            aircraft.setName((String) ((ArrayList) o).get(0));
            Position position = new Position();
            if (((ArrayList) o).get(5) instanceof Double) {
                position.setLongitude((Double) ((ArrayList) o).get(5));
            } else if (((ArrayList) o).get(5) instanceof Integer) {
                position.setLongitude(((Integer) ((ArrayList) o).get(5)).doubleValue());
            }
            if (((ArrayList) o).get(6) instanceof Double) {
                position.setLatitude((Double) ((ArrayList) o).get(6));
            } else if (((ArrayList) o).get(6) instanceof Integer) {
                position.setLatitude(((Integer) ((ArrayList) o).get(6)).doubleValue());
            }
            aircraft.setPosition(position);
            aircraft.setOnGround((Boolean) ((ArrayList) o).get(8));

            aircraftsList.add(aircraft);
        }

        return new ResponseEntity<Response>(ResponseFactory.success(aircraftsList), HttpStatus.OK);
    }

    @RequestMapping(value = "/aircraft/{name}", method = RequestMethod.GET)
    public ResponseEntity requestAircraftPositions(@PathVariable String name) {

        String url = "https://opensky-network.org/api/states/all?icao24={name}";

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, Object.class, name);
        HttpStatus statusCode = responseEntity.getStatusCode();
        if (statusCode.value() != 200) {
            return responseEntity;
        }
        Object objects = responseEntity.getBody();
        JSONObject object = new JSONObject((Map) objects);

        if (object.get("states") == null) {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("Aircraft not found."), HttpStatus.BAD_REQUEST);
        }

        JSONArray aircrafts = new JSONArray();
        aircrafts.addAll((ArrayList) (object.get("states")));

        Object buffor = aircrafts.get(0);
        Aircraft aircraft = new Aircraft();
        aircraft.setName((String) ((ArrayList) buffor).get(0));
        Position position = new Position();
        if (((ArrayList) buffor).get(5) instanceof Double) {
            position.setLongitude((Double) ((ArrayList) buffor).get(5));
        } else if (((ArrayList) buffor).get(5) instanceof Integer) {
            position.setLongitude(((Integer) ((ArrayList) buffor).get(5)).doubleValue());
        }
        if (((ArrayList) buffor).get(6) instanceof Double) {
            position.setLatitude((Double) ((ArrayList) buffor).get(6));
        } else if (((ArrayList) buffor).get(6) instanceof Integer) {
            position.setLatitude(((Integer) ((ArrayList) buffor).get(6)).doubleValue());
        }
        aircraft.setPosition(position);
        aircraft.setOnGround((Boolean) ((ArrayList) buffor).get(8));

        return new ResponseEntity<Response>(ResponseFactory.success(aircraft), HttpStatus.OK);
    }

}
