package com.ag.flightscanner.controllers;

import com.ag.flightscanner.model.responses.Response;
import com.ag.flightscanner.model.responses.ResponseFactory;
import com.ag.flightscanner.model.users.AuthenticationData;
import com.ag.flightscanner.model.users.AuthenticationPassword;
import com.ag.flightscanner.model.users.User;
import com.ag.flightscanner.model.users.UserData;
import com.ag.flightscanner.services.UserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

@Controller
@RequestMapping(value = "/thickview/")
public class IndexControllerThick {

    @Autowired
    UserService service;

    @RequestMapping(value = "/getUserList")
    public String getUserList() {
        return "UserList";
    }

    @RequestMapping(value = "/homepage")
    public String getHomepage() {
        return "HomepageThick";
    }

    @RequestMapping(value = "/schedule")
    public String getFlightList() {
        return "FlightList";
    }

    @RequestMapping(value = "/userProfile")
    public String getUserProfile(ModelMap model) {
        return "UserProfileThick";
    }

    @RequestMapping(value = "/userProfile/settings")
    public String getUserSettings(ModelMap model) {
        return "UserSettingsThick";
    }

    @RequestMapping(value = "/userProfile/savedFlights")
    public String getUserSavedFlights(ModelMap model) {
        return "UserSavedFlights";
    }

    @RequestMapping(value = "/userProfile/flights")
    public String getUserFlights(ModelMap model) {
        return "UserFlights";
    }

    @RequestMapping(value = "/register")
    public String getRegisterForm(ModelMap model) {
        return "UserForm";
    }

    @RequestMapping(value = "/logout")
    public String tryLogout(ModelMap model) {
        return "Logout";
    }

    @RequestMapping(value = "/followPlane")
    public String getAircraftPosition(ModelMap model) {
        return "FollowPlane";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String tryLogin(ModelMap model, @ModelAttribute AuthenticationData authenticationData) {

        Optional<User> userOptional = service.getUserByLogin(authenticationData.getLogin());
        if (!userOptional.isPresent()) {
            model.addAttribute("notification", "Invalid login");
            return "UserForm";
        }

        User user = userOptional.get();
        if (!service.confirmPassword(user, authenticationData.getPassword())) {
            model.addAttribute("notification", "Invalid password");
            return "UserForm";
        }
        String keySession = RandomStringUtils.randomAlphanumeric(30);
        model.addAttribute("sessionId", keySession);
        model.addAttribute("userId", user.getId());
        model.addAttribute("remember", authenticationData.isRemember());
        return "LoginSuccessful";
    }

    @RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
    public String updatePassword(@ModelAttribute AuthenticationPassword authenticationPassword,
                                 ModelMap model) {
        Logger.getLogger(getClass()).debug("Requested updatePassword");

        Optional<User> optionalUser = service.getUserById(authenticationPassword.getUserId());
        if (!optionalUser.isPresent()) {
            model.addAttribute("notification", "Update failed. User with that id does not exist.");
        }

        User user = optionalUser.get();
        if (service.confirmPassword(user, authenticationPassword.getOldPassword())) {
            user.setPasswordHash(DigestUtils.md5Hex(authenticationPassword.getNewPassword()));
            service.updateUser(user);
        } else {
            model.addAttribute("notification", "Updating failed. Wrong password.");
        }
        model.addAttribute("userid", authenticationPassword.getUserId());
        return "UserSettingsThick";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String deleteUser(ModelMap model,
                             @ModelAttribute String password,
                             @ModelAttribute Long userId) {
        Logger.getLogger(getClass()).debug("Requested deleteUser");

        Optional<User> userOptional = service.getUserById(userId);
        if (!userOptional.isPresent()) {
            model.addAttribute("notification", "Delete failed. User with that id does not exist.");
            return "UserSettingsThick";
        }
        User user = userOptional.get();
        if (service.confirmPassword(user, password)) {
            service.deleteUser(user);
            return "HomepageThick";
        } else {
            model.addAttribute("notification", "Delete failed. Wrong password.");
        }
        model.addAttribute("uderid", userId);
        return "UserSettingsThick";
    }
}
