package com.ag.flightscanner.controllers;

import com.ag.flightscanner.model.journey.Schedule;
import com.ag.flightscanner.model.responses.Response;
import com.ag.flightscanner.model.responses.ResponseFactory;
import com.ag.flightscanner.model.users.User;
import com.ag.flightscanner.services.ScheduleService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;

@RestController
public class ScheduleController {

    private static final Logger LOG = Logger.getLogger(ScheduleController.class);

    @Autowired
    ScheduleService service;

    @RequestMapping(value = "/addSchedule", method = RequestMethod.POST)
    public ResponseEntity<Schedule> requestAddSchedule(@RequestBody Schedule schedule) {
        Logger.getLogger(getClass()).debug("Request addSchedule");

        Boolean result = service.addSchedule(schedule);

        if (!result) {
            return new ResponseEntity<Schedule>(schedule, HttpStatus.BAD_REQUEST);

        } else {
            return new ResponseEntity<Schedule>(schedule, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/schedules", method = RequestMethod.GET)
    public ResponseEntity requestGetSchedule() {
        Logger.getLogger(getClass()).debug("Request getSchedule");

        return new ResponseEntity<Iterable<Schedule>>(service.getAllSchedule(), HttpStatus.OK);
    }


    @RequestMapping(value = "/LH/schedules/origin/{origin}/destination/{destination}/data/{data}", method = RequestMethod.GET)
    public ResponseEntity requestLufthansaFlightList(@PathVariable String origin,
                                                     @PathVariable String destination,
                                                     @PathVariable String data,
                                                     @RequestParam boolean direct) {

        Map<String, String> vars = new HashMap<>();
        vars.put("origin", origin);
        vars.put("destination", destination);
        vars.put("date", data);
        if (direct == true) {
            vars.put("direct", "1");
        } else {
            vars.put("direct", "0");
        }

        String url = "https://api.lufthansa.com/v1/operations/schedules/{origin}/{destination}/{date}?directFlights={direct}";

        ResponseEntity responseEntity = service.importFlights(url, vars);
        HttpStatus statusCode = responseEntity.getStatusCode();
        if (statusCode.value() != 200) {
            return responseEntity;
        }

        Object objects = responseEntity.getBody();
        JSONObject object = new JSONObject((Map) objects);
        JSONObject scheduleResource = new JSONObject((Map) (object.get("ScheduleResource")));
        JSONArray schedules = new JSONArray();
        schedules.addAll((ArrayList) (scheduleResource.get("Schedule")));

        Gson g = new Gson();
        Type listType = new TypeToken<List<Schedule>>() {
        }.getType();
//        List<Schedule> scheduleList = g.fromJson(schedules.toJSONString(), listType);
        ObjectMapper mapper = new ObjectMapper();
        List<Schedule> scheduleList = null;
        try {
            scheduleList = Arrays.asList(mapper.readValue( schedules.toJSONString(), Schedule[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        service.addSchedules(scheduleList);

//        return new ResponseEntity<List<Schedule>>(scheduleList, HttpStatus.OK);
        return new ResponseEntity<Response>(ResponseFactory.success(scheduleList), HttpStatus.OK);

    }

}
