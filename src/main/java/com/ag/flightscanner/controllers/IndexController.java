package com.ag.flightscanner.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/")
public class IndexController {

    @RequestMapping(value = "/getUserList")
    public String getUserList(){
        return "UserList";
    }
}
