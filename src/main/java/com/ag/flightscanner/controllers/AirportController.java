package com.ag.flightscanner.controllers;

import com.ag.flightscanner.model.journey.Airport;
import com.ag.flightscanner.services.AirportService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/rest")
public class AirportController {

    private static final Logger LOG = Logger.getLogger(AirportController.class);

    @Autowired
    AirportService service;

    @RequestMapping(value = "/addAirport", method = RequestMethod.POST)
    public ResponseEntity<Airport> requestAddAirport(@RequestBody Airport airport) {
        Logger.getLogger(getClass()).debug("Request addAirport");

        service.addAirport(airport);
        return new ResponseEntity<Airport>(airport, HttpStatus.OK);
    }

    @RequestMapping(value = "/LH/airports", method = RequestMethod.GET)
    public ResponseEntity requestLufthansaAirportsList() {


        String url = "https://api.lufthansa.com/v1/references/airports/?limit=100&offset=0&LHoperated=1";

        ResponseEntity responseEntity = service.importAirports(url);
        HttpStatus statusCode = responseEntity.getStatusCode();
        if (statusCode.value() != 200) {
            return responseEntity;
        }

        Object objects = responseEntity.getBody();
        JSONObject object = new JSONObject((Map) objects);
        JSONObject airportResource = new JSONObject((Map) (object.get("AirportResource")));
        JSONObject airports = new JSONObject((Map) (airportResource.get("Airports")));
        JSONArray airport = new JSONArray();
        airport.addAll((ArrayList) (airports.get("Airport")));

        Gson g = new Gson();
        Type listType = new TypeToken<List<Airport>>() {
        }.getType();

        ObjectMapper mapper = new ObjectMapper();
        List<Airport> airportsList = null;
        try {
            airportsList = Arrays.asList(mapper.readValue(airport.toJSONString(), Airport[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }

        service.addAirports(airportsList);

        return new ResponseEntity<List<Airport>>((List<Airport>) airportsList, HttpStatus.OK);
    }

    @RequestMapping(value = "/getAirports", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Airport>> requestAirportList() {
        return new ResponseEntity<Iterable<Airport>>(service.getAllAirports(), HttpStatus.OK);
    }

}

