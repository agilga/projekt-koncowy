package com.ag.flightscanner.dao;

import com.ag.flightscanner.model.users.User;
import com.ag.flightscanner.model.users.UserData;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository(value = "userDao")
public class UserDaoImpl extends AbstractDao implements UserDao {

    @Override
    public Optional<User> findById(Long id) {
        Object result = getSession().createCriteria(User.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();
        return Optional.ofNullable((User) result);
    }

    @Override
    public boolean existsByLogin(String login) {
        long resultCount = (long) getSession().createCriteria(User.class)
                .add(Restrictions.eq("login", login).ignoreCase())
                .setProjection(Projections.rowCount())
                .uniqueResult();
        return resultCount > 0;
    }

    @Override
    public Optional<User> findByLogin(String login) {
        Object result = getSession().createCriteria(User.class)
//                .createAlias("UserData", "userData")
                .add(Restrictions.eq("login", login))
                .uniqueResult();
        return Optional.ofNullable((User) result);
    }

    public Iterable<User> findAll() {
        return getSession().createCriteria(User.class).list();
    }

    @Override
    public void registerUser(User u, UserData data) {
        save(data);
        u.setUserData(data);
        save(u);
    }


}
