package com.ag.flightscanner.dao;

import com.ag.flightscanner.model.journey.Airport;
import com.ag.flightscanner.model.journey.Flight;

import java.time.LocalDateTime;

public interface FlightDao {

    Iterable<Flight> findAllByOriginPlaceAndDestinationPlace(Airport originPlace, Airport destinationAirport);

    Iterable<Flight> findAllByOriginPlaceAndDestinationPlaceAAndOriginDateTimeAndDestinationDateTime(Airport originPlace,
                                                                                                     Airport destinationAirport,
                                                                                                     LocalDateTime originDateTime,
                                                                                                     LocalDateTime destinationDateTime);
    Iterable<Flight> findAllByOriginDateTimeAndDestinationDateTime(LocalDateTime originDateTime,
                                                                   LocalDateTime destinationDateTi);

}
