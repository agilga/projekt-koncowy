package com.ag.flightscanner.dao;

import com.ag.flightscanner.model.journey.Airport;

public interface AirportDao {

    Iterable<Airport> findAll();

    boolean existsByCode(String code);

    }
