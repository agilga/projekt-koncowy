package com.ag.flightscanner.dao;

import com.ag.flightscanner.model.journey.Airport;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository(value = "airportDao")
@NoRepositoryBean
public class AirportDaoImpl extends AbstractDao implements AirportDao {

    @Override
    public Iterable<Airport> findAll() {
        return getSession().createCriteria(Airport.class).list();
    }

    public Optional<Airport> findByAirportCode(String airportCode) {
        Object result = getSession().createCriteria(Airport.class)
                .add(Restrictions.eq("AirportCode", airportCode))
                .uniqueResult();
        return Optional.ofNullable((Airport) result);
    }

    @Override
    public boolean existsByCode(String code) {
        long resultCount = (long) getSession().createCriteria(Airport.class)
                .add(Restrictions.eq("AirportCode", code).ignoreCase())
                .setProjection(Projections.rowCount())
                .uniqueResult();
        return resultCount > 0;
    }
}
