package com.ag.flightscanner.dao;

import com.ag.flightscanner.model.journey.Airport;
import com.ag.flightscanner.model.journey.Flight;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;


@Repository(value = "flightDao")
@NoRepositoryBean
public class FlightDaoImpl extends AbstractDao implements FlightDao {
    @Override
    public Iterable<Flight> findAllByOriginPlaceAndDestinationPlace(Airport originPlace, Airport destinationPlace) {
        return getSession().createCriteria(Flight.class)
                .add(Restrictions.eq("originPlace", originPlace))
                .add(Restrictions.eq("destinationPlace", destinationPlace))
                .list();
    }

    @Override
    public Iterable<Flight> findAllByOriginPlaceAndDestinationPlaceAAndOriginDateTimeAndDestinationDateTime(Airport originPlace, Airport destinationPlace, LocalDateTime originDateTime, LocalDateTime destinationDateTime) {
        return getSession().createCriteria(Flight.class)
                .add(Restrictions.eq("originPlace", originPlace))
                .add(Restrictions.eq("destinationPlace", destinationPlace))
                .add(Restrictions.eq("originDateTime", originDateTime))
                .add(Restrictions.eq("destinationDateTime", destinationDateTime))
                .list();
    }

    @Override
    public Iterable<Flight> findAllByOriginDateTimeAndDestinationDateTime(LocalDateTime originDateTime, LocalDateTime destinationDateTime) {
        return getSession().createCriteria(Flight.class)
                .add(Restrictions.eq("originDateTime", originDateTime))
                .add(Restrictions.eq("destinationDateTime", destinationDateTime))
                .list();
    }

    public Iterable<Flight> findAll() {
        return getSession().createCriteria(Flight.class).list();
    }
}
