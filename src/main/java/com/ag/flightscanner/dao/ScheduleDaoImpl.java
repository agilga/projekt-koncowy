package com.ag.flightscanner.dao;


import com.ag.flightscanner.model.journey.Schedule;
import com.ag.flightscanner.model.users.User;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository(value = "scheduleDao")
@NoRepositoryBean
public class ScheduleDaoImpl extends AbstractDao implements ScheduleDao {
    public Iterable<Schedule> findAll() {
        return getSession().createCriteria(Schedule.class).list();
    }

    @Override
    public Optional<Schedule> findById(Long id) {
        Object result = getSession().createCriteria(Schedule.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();
        return Optional.ofNullable((Schedule) result);

    }
}
