package com.ag.flightscanner.dao;


import com.ag.flightscanner.model.users.User;
import com.ag.flightscanner.model.users.UserData;

import java.util.Optional;

public interface UserDao {

    Optional<User> findById(Long id);
    boolean existsByLogin(String login);
    void registerUser(User u, UserData data);
    Optional<User> findByLogin(String login);

}
