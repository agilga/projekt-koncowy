package com.ag.flightscanner.dao;

import com.ag.flightscanner.model.journey.Airport;
import com.ag.flightscanner.model.journey.Flight;
import com.ag.flightscanner.model.journey.Schedule;

import java.time.LocalDateTime;
import java.util.Optional;

public interface ScheduleDao {

    Optional<Schedule> findById(Long id);

}
