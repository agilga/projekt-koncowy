<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>FlightScanner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-cookies.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">

    <script src="http://code.jquery.com/jquery.min.js"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">--%>
    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">--%>
    <%--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>--%>


    <style>
        body {
            padding-top: 5rem;
        }

        .vertical-center {
            min-height: 100%;
            min-height: 100vh;

            display: flex;
            align-items: center;
            align-content: center;
        }

        .help-block {
            color: cadetblue;
            size: 10;
        }

        .with-errors {
            color: #c9001a;
            size: 10;
        }
    </style>

</head>

<body ng-app="flightapp">
<%@ include file="navbar.jsp" %>


<div class="vertical-center" ng-controller="AircraftController as ctrl">
    <div class="row container">

        <div class="col-md-4 col-md-offset-3 panel panel-offline-inverse">

            <form ng-submit="ctrl.fetchAircraftWithName(name)" data-toggle="validator" role="form">
                <div class="form-group">
                    <input type="text" class="form-control" id="name" ng-model="name" placeholder="Aircraft number"
                           data-error="That plane number doesn't exist" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Find</button>
                </div>
            </form>
        </div>

        <div class="container col-md-7">
            <div id="map" style="height:500px;"></div>
        </div>

    </div>


    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwGIklRYTk_IsQJgnxsDNvvjLPj9OlV7o&callback=initMap">
    </script>

    <script type="application/javascript">
        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 2,
                center: new google.maps.LatLng(0, 0),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
        }
    </script>
</div>

<script src="/static/js/app.js"></script>
<script src="/static/js/services/user_service.js"></script>
<script src="/static/js/controllers/user_controller.js"></script>
<script src="/static/js/services/aircraft_service.js"></script>
<script src="/static/js/controllers/aircraft_controller.js"></script>
<script src="/static/js/controllers/session_controller.js"></script>


</body>
</html>
