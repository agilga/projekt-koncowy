<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>UserList</title>
</head>
<body ng-app="flightapp">
<h1>This is List of users</h1>
<div ng-controller="UserController as ctrl">
    <div ng-repeat="user in ctrl.users">
        <span ng-bind="user.id"></span>
        <span ng-bind="user.login"></span>
        <span ng-bind="user.passwordHash"></span>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
<script src="/static/js/app.js"></script>
<script src="/static/js/services/user.service.js"></script>
<script src="/static/js/controllers/user.controller.js"></script>
</body>
</html>
