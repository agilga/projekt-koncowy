<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="http://localhost:8080/thickview/homepage">FlightScanner</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault" ng-controller="SessionController as session">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="http://localhost:8080/thickview/followPlane">Follow the plane</a>
            </li>
        </ul>
        <div data-ng-init="isLoggedInForNavbar()"></div>

        <ul class="nav navbar-nav navbar-right">
            <li class="nav-item" ng-if="!session.loggedIn">
                <a class="nav-link" href="http://localhost:8080/thickview/register"><i class="material-icons">account_circle</i>
                    Log in</a>
            </li>

            <li class="nav-item dropdown" ng-if="session.loggedIn">
                <a class="nav-link dropdown-toggle" href="" id="dropdownMenu" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false"><i class="material-icons">account_circle</i>
                    My account
                </a>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu">
                    <li><a class="dropdown-item" href="http://localhost:8080/thickview/userProfile">My profile</a></li>
                    <li><a class="dropdown-item" href="http://localhost:8080/thickview/logout">Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
