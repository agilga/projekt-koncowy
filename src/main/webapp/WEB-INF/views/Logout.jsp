<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018-01-12
  Time: 23:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-cookies.js"></script>
    <style>
        body {
            background-color: #305c87;
            color: #95a7b7;
            text-align: center;
            align-content: center;
        }
    </style>
</head>
<body ng-app="flightapp">
<div>
    <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
    <span class="sr-only">Logging out</span>
</div>
<div class="container" ng-controller="SessionController as ctrl">
    <div data-ng-init="logout('${sessionId}')">
        ${sessionId}
    </div>
</div>

<script src="/static/js/app.js"></script>
<script src="/static/js/controllers/session_controller.js"></script>
</body>
</html>
