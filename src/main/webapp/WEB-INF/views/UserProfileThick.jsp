<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-cookies.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"
            integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"
            integrity="sha256-xI/qyl9vpwWFOXz7+x/9WkG5j/SVnSw21viy8fWwbeE=" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="/static/css/user_profile.css">
    <link rel="stylesheet" href="/static/css/sidebar.css">
    <link rel="stylesheet" href="/static/css/footer.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
            integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
            integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
            crossorigin="anonymous"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
</head>
<body ng-app="flightapp">
<div id="sessionDiv" ng-controller="SessionController as session">
    <div data-ng-init="isLoggedIn()">
    </div>
    <div data-ng-init="currentUser()"></div>
</div>

<head>
    <%@ include file="navbar.jsp" %>
</head>

<div class="container-fluid " ng-controller="SessionController as session">
    <%@ include file="sidebar.jsp" %>

    <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3 main-content">
        <div ng-controller="UserController as ctrl">
            <div data-ng-init="onLoadFunction()">
                <div class="header">
                    <h1 ng-bind="ctrl.user.login"></h1>
                </div>
                <div class="container col-6 col-md-6">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>First name</td>
                            <td ng-bind="ctrl.user.userData.firstName"></td>
                        </tr>
                        <tr>
                            <td>Last name</td>
                            <td ng-bind="ctrl.user.userData.lastName"></td>
                        </tr>
                        <tr>
                            <td>e-mail</td>
                            <td ng-bind="ctrl.user.userData.email"></td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td><span ng-bind="ctrl.user.userData.address.street"> </span>
                                <span ng-bind="ctrl.user.userData.address.houseNumber"> </span></br>
                                <span ng-bind="ctrl.user.userData.address.zipCode"> </span>
                                <span ng-bind="ctrl.user.userData.address.city"> </span>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <button class="btn btn-info" type="button" onclick="window.location.href='userProfile/settings'">Edit profile
                    </button>
                </div>
            </div>
        </div>
    </main>
</div>
<script src="/static/js/app.js"></script>
<script src="/static/js/services/user_service.js"></script>
<script src="/static/js/controllers/user_controller.js"></script>
<script src="/static/js/controllers/session_controller.js"></script>
</body>

</html>
