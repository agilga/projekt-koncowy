<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-cookies.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"
            integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"
            integrity="sha256-xI/qyl9vpwWFOXz7+x/9WkG5j/SVnSw21viy8fWwbeE=" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="/static/css/user_profile.css">
    <link rel="stylesheet" href="/static/css/sidebar.css">
    <link rel="stylesheet" href="/static/css/footer.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
            integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
            integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
            crossorigin="anonymous"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">

    <style>
        .panel {
            border-width: 5px;
        }
    </style>
</head>
<body ng-app="flightapp">
<div id="sessionDiv" ng-controller="SessionController as session">
    <div data-ng-init="isLoggedIn()">
    </div>
    <div data-ng-init="currentUser()"></div>
</div>
<head>
    <%@ include file="navbar.jsp" %>
</head>

<div class="container-fluid " ng-controller="SessionController as session">
    <%@ include file="sidebar.jsp" %>

    <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3 main-content">
        <div ng-controller="UserController as ctrl">
            <div data-ng-init="onLoadFunction()">
                <div class="header align-self-center">
                    <h1>Settings</h1>
                </div>

                <div class="col-md-10 align-self-center">
                    <div class="panel-group" id="accordion">
                        <div class="card card-info">
                            <div class="card-header">
                                <a data-toggle="collapse" data-parent="#accordion" href="#edit">
                                    Edit profile</a>
                            </div>
                            <div id="edit" class="card-collapse collapse">
                                <div class="card-body">
                                    <form data-toggle="validator" role="form" ng-submit="ctrl.updateUser()">
                                        <div class="form-group">
                                            <label for="inputFirstName" class="control-label">First name</label>
                                            <input type="text" class="form-control" name="firstName" id="inputFirstName"
                                                   ng-model="ctrl.userData.firstName"
                                                   ng-value="ctrl.user.userData.firstName">
                                            <div class="help-block with-errors"></div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputLastName" class="control-label">Last name</label>
                                            <input type="text" class="form-control" name="lastName" id="inputLastName"
                                                   ng-model="ctrl.userData.lastName"
                                                   ng-value="ctrl.user.userData.lastName"
                                            >
                                            <div class="help-block with-errors"></div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputEmail" class="control-label">Email</label>
                                            <input type="email" class="form-control" name="email" id="inputEmail"
                                                   ng-model="ctrl.userData.email"
                                                   ng-value="ctrl.user.userData.email"
                                                   data-error="That email address is invalid">
                                            <div class="help-block with-errors"></div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputStreet" class="control-label">Street</label>
                                            <input type="text" class="form-control" name="address.street"
                                                   id="inputStreet"
                                                   ng-model="ctrl.userData.address.street"
                                                   ng-value="ctrl.user.userData.address.street"
                                            >
                                            <div class="help-block with-errors"></div>

                                            <label for="inputHouseNumber" class="control-label">House number</label>
                                            <input type="text" class="form-control" name="address.houseNumber"
                                                   id="inputHouseNumber"
                                                   ng-model="ctrl.userData.address.houseNumber"
                                                   ng-value="ctrl.user.userData.address.houseNumber"
                                            >
                                            <div class="help-block with-errors"></div>

                                            <label for="inputCity" class="control-label">City</label>
                                            <input type="text" class="form-control" name="address.city" id="inputCity"
                                                   ng-model="ctrl.userData.address.city"
                                                   ng-value="ctrl.user.userData.address.city"
                                            >
                                            <div class="help-block with-errors"></div>

                                            <label for="inputZipCode" class="control-label">Zip code</label>
                                            <input type="text" class="form-control" name="address.zipCode"
                                                   id="inputZipCode"
                                                   ng-model="ctrl.userData.address.zipCode"
                                                   ng-value="ctrl.user.userData.address.zipCode"
                                            >
                                            <div class="help-block with-errors"></div>

                                            <label for="inputCountry" class="control-label">Country</label>
                                            <input type="text" class="form-control" name="address.country"
                                                   id="inputCountry"
                                                   ng-model="ctrl.userData.address.country"
                                                   ng-value="ctrl.user.userData.address.country"
                                            >
                                            <div class="help-block with-errors"></div>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Edit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="card card-info">
                            <div class="card-header">
                                <a data-toggle="collapse" data-parent="#accordion" href="#change-password">
                                    Change password</a>
                            </div>
                            <div id="change-password" class="card-collapse collapse">
                                <div class="card-body">
                                    <form data-toggle="validator" method="POST" action="/thickview/updatePassword">
                                        <input type="hidden" name="userId" ng-value="ctrl.user.id">
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="oldPassword"
                                                   name="oldPassword"
                                                   placeholder="Old password" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" data-minlength="8" class="form-control"
                                                   id="newPassword" name="newPassword"
                                                   placeholder="New password" required>
                                            <span class="help-block">Minimum of 8 characters</span>
                                        </div>
                                        <div>
                                            <input type="password" class="form-control" id="passwordConfirm"
                                                   name="passwordConfirm"
                                                   data-match="#newPassword" data-match-error="These don't match"
                                                   placeholder="Confirm new password" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-info">Change password</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="card card-info">
                            <div class="card-header">
                                <a data-toggle="collapse" data-parent="#accordion" href="#delete">
                                    Delete account</a>
                            </div>
                            <div id="delete" class="card-collapse collapse">
                                <div class="card-body">
                                    <form data-toggle="validator" role="form" method="post" action="/thickview/delete/">
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="password" name="password"
                                                   placeholder="Password" required>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-info">Delete my account</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<script src="/static/js/app.js"></script>
<script src="/static/js/services/user_service.js"></script>
<script src="/static/js/controllers/user_controller.js"></script>
<script src="/static/js/controllers/session_controller.js"></script>
</body>


</html>
