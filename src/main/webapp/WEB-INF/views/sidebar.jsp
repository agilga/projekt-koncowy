<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<nav>
    <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light nav-side-menu">
        <div class="brand">My Account</div>
        <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

        <div class="menu-list">

            <ul id="menu-content" class="menu-content collapse out">
                <li>
                    <a href="/thickview/userProfile">
                        <i class="fa fa-user fa-lg"></i> Profile
                    </a>
                </li>
                <li>
                    <a href="/thickview/userProfile/flights">
                        <i class="fa fa-plane fa-lg"></i> My flights
                    </a>
                </li>
                <li>
                    <a href="/thickview/userProfile/savedFlights">
                        <i class="fa fa-bookmark fa-lg"></i> Saved flights
                    </a>
                </li>
                <li>
                    <a href="/thickview/userProfile/settings">
                        <i class="fa fa-cog fa-lg"></i> Settings
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<%--<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">--%>

