<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div>
    <div class="container" ng-if="ctrl.flightInfo!=null">
        <div ng-if="ctrl.schedules==null">
            <div class="alert alert-danger text-center" role="alert">
                <span ng-bind="ctrl.flightInfo"></span>
            </div>
        </div>
    </div>

    <div ng-repeat="schedule in ctrl.schedules | orderBy : 'Flight[0].Departure.ScheduledTimeLocal.DateTime'">
        <div class="container">
            <div class="card card-default col-md-12 col-12">
                <div class="row card-header text-center">
                    <div class="col-md-3">
                        <h4>{{schedule.Flight[0].Departure.ScheduledTimeLocal.DateTime.split('T').join(' ')}}</h4>
                        <span>{{schedule.Flight[0].Departure.AirportCode}}</span>
                    </div>
                    <div class="col-md-3">
                        <img src="http://www.pvhc.net/img25/vimvljritqrpvlgwjidd.png"
                             style=" height: 20px; horizontal-align: center">
                        <hr width="200"/>
                        <span>stops: <span ng-bind="schedule.Flight.length-1"></span></span>
                    </div>
                    <div class="col-md-3">
                        <h4>
                            {{schedule.Flight[schedule.Flight.length-1].Arrival.ScheduledTimeLocal.DateTime.split('T').join('
                            ')}}</h4>
                        <span>{{schedule.Flight[schedule.Flight.length-1].Arrival.AirportCode}}</span>
                    </div>

                    <div class="col-md-2" ng-controller="SessionController as session">
                        <div data-ng-init="isLoggedInForNavbar()"></div>
                        <div data-ng-init="currentUser()"></div>
                        <div ng-if="session.loggedIn">
                            <p ng-bind="schedule.TotalJourney.price">Price</p>
                            <button class="btn btn-outline-info"
                                    ng-click="ctrl.buyJourney(schedule.id, session.userId)">Buy
                            </button>
                            <button class="btn btn-outline-info"
                                    ng-click="ctrl.saveJourney(schedule.id, session.userId)">Save
                            </button>
                        </div>
                        <div ng-if="!session.loggedIn">
                            <p ng-bind="schedule.TotalJourney.price">Price</p>
                            <a role=button class="btn btn-outline-info" href="https://www.lufthansa.com/">Buy</a>
                        </div>
                    </div>

                </div>
                <div class="panel-body">
                    <table class="table table-condensed">
                        <thead>
                        <tr>
                            <th>Aircraft code</th>
                            <th>Departure airport</th>
                            <th>Departure date</th>
                            <th>Arrival airport</th>
                            <th>Arrival date</th>
                        </tr>
                        </thead>
                        <tbody ng-repeat="f in schedule.Flight">
                        <tr>
                            <td>{{f.Equipment.AircraftCode}}</td>
                            <td>{{f.Departure.AirportCode}}</td>
                            <td>{{f.Departure.ScheduledTimeLocal.DateTime.split('T').join(' ')}}</td>
                            <td>{{f.Arrival.AirportCode}}</td>
                            <td>{{f.Arrival.ScheduledTimeLocal.DateTime.split('T').join(' ')}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
