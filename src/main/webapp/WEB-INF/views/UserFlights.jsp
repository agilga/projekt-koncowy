<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-cookies.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"
            integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"
            integrity="sha256-xI/qyl9vpwWFOXz7+x/9WkG5j/SVnSw21viy8fWwbeE=" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="/static/css/user_profile.css">
    <link rel="stylesheet" href="/static/css/sidebar.css">
    <link rel="stylesheet" href="/static/css/footer.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
            integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
            integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
            crossorigin="anonymous"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">

</head>


<body ng-app="flightapp">

<div id="sessionDiv" ng-controller="SessionController as session">
    <div data-ng-init="isLoggedIn()">
    </div>
    <div data-ng-init="currentUser()"></div>
</div>
<head>
    <%@ include file="navbar.jsp" %>
</head>

<div class="container-fluid " ng-controller="SessionController as session">
    <%@ include file="sidebar.jsp" %>

    <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3 main-content">
        <div ng-controller="UserController as ctrl">
            <div data-ng-init="onLoadFunction()">
                <div class="header">
                    <h1>My flights</h1>
                </div>
                <div class="container col-10 col-md-10">
                    <table class="table table-hover text-center">
                        <thead>
                        <tr>
                            <th>Departure airport</th>
                            <th>Departure date</th>
                            <th>Arrival airport</th>
                            <th>Arrival date</th>
                            <th>Payment status</th>
                        </tr>
                        </thead>
                        <tbody ng-repeat="purchasedFlights in ctrl.user.purchasedFlights | orderBy : 'schedule.Flight[0].Departure.ScheduledTimeLocal.DateTime'">
                        <tr class="table-active">
                            <td>{{purchasedFlights.schedule.Flight[0].Departure.AirportCode}}</td>
                            <td>{{purchasedFlights.schedule.Flight[0].Departure.ScheduledTimeLocal.DateTime.split('T').join(' ');}}</td>
                            <td>{{purchasedFlights.schedule.Flight[schedule.Flight.length-1].Arrival.AirportCode}}</td>
                            <td>{{purchasedFlights.schedule.Flight[schedule.Flight.length-1].Arrival.ScheduledTimeLocal.DateTime.split('T').join(' ');}}</td>
                            <td ng-if="purchasedFlights.paymentStatus"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td ng-if="!purchasedFlights.paymentStatus"><i class="fa fa-times" aria-hidden="true"></i></td>

                            <td>
                                <div class="btn-group" ng-controller="JourneyController as journeyCtrl">
                                    <button type="button" class="btn btn-info dropdown-toggle"
                                            data-toggle="dropdown">
                                        Action </span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Get ticket</a></li>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <tr class="tab-content">
                            <td colspan="5">
                                <div>
                                    <table class="table text-center">
                                        <thead>
                                        <tr>
                                            <th>Aircraft code</th>
                                            <th>Departure airport</th>
                                            <th>Departure date</th>
                                            <th>Departure terminal</th>
                                            <th>Arrival airport</th>
                                            <th>Arrival date</th>
                                            <th>Arrival terminal</th>
                                        </tr>
                                        </thead>
                                        <tbody ng-repeat="f in purchasedFlights.schedule.Flight">
                                        <tr>
                                            <td>{{f.Equipment.AircraftCode}}</td>
                                            <td>{{f.Departure.AirportCode}}</td>
                                            <td>{{f.Departure.ScheduledTimeLocal.DateTime.split('T').join(' ')}}</td>
                                            <td>{{f.Departure.Terminal.Name}}</td>
                                            <td>{{f.Arrival.AirportCode}}</td>
                                            <td>{{f.Arrival.ScheduledTimeLocal.DateTime.split('T').join(' ')}}</td>
                                            <td>{{f.Arrival.Terminal.Name}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
</div>

<script src="/static/js/app.js"></script>
<script src="/static/js/services/user_service.js"></script>
<script src="/static/js/controllers/user_controller.js"></script>
<script src="/static/js/services/journey_service.js"></script>
<script src="/static/js/controllers/journey_controller.js"></script>
<script src="/static/js/controllers/session_controller.js"></script>
</body>

</html>

