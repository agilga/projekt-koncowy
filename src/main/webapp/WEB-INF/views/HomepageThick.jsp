<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>FlightScanner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-cookies.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"--%>
    <%--integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"--%>
    <%--crossorigin="anonymous"></script>--%>
    <%--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"--%>
    <%--integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4"--%>
    <%--crossorigin="anonymous"></script>--%>
    <%--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"--%>
    <%--integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"--%>
    <%--crossorigin="anonymous"></script>--%>

    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"
            integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"
            integrity="sha256-xI/qyl9vpwWFOXz7+x/9WkG5j/SVnSw21viy8fWwbeE=" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">

    <script type="application/javascript">
        $(document).ready(function () {
            $('.datepicker').datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: 0,
                onSelect: function (date) {
                    angular.element($('.datepicker')).triggerHandler('input');
                }
            });
        });
    </script>

    <style>
        #home {
            background-image: url('https://images.studentuniverse.com/new/suwebui/photos/top/plane-header.jpg');
            text-align: center;
            height: 440px;
            padding-top: 100px;
        }

        .browser {
            padding: 20px;
            background-color: rgba(217, 217, 217, 0.5);
            text-align: center;
            box-align: center;
        }

        .element {
            margin-bottom: 30px;
            margin-top: 30px;
        }

        #daterange {
            visibility: hidden;
        }

    </style>

</head>

<body ng-app="flightapp">
<%@ include file="navbar.jsp" %>

<main role="main" ng-controller="JourneyController as ctrl">
    <div class="jumbotron" id="home">
        <div class="container-fluid text-center center-block">
            <div class="container browser col-md-11 rounded">
                <div class="row">
                    <label class="radio-inline">
                        <input type="radio" onclick="javascript:journey();" name="travelOptions" id="return"
                               value="return"> Return
                    </label>
                    <label class="radio-inline">
                        <input type="radio" onclick="javascript:journey();" name="travelOptions" id="one-way"
                               value="one-way" checked="checked"> One-way
                    </label>
                </div>
                <div class="row" ng-controller="AirportController as ctrlAirport">
                    <form class="form-inline" id="journeyForm" ng-submit="ctrl.findJourney()">
                        <div class="input-group ">
                            <select name="From" class="selectpicker" data-live-search="true"
                                    ng-model="ctrl.newJourney.from">
                                <option disabled selected value> From</option>
                                <option ng-repeat="airport in ctrlAirport.airports | orderBy : 'Names.en'"
                                        ng-value="airport.AirportCode">
                                    {{airport.Names.en}}
                                </option>
                            </select>
                            <span class="input-group-addon"><i class="material-icons">room</i></span>
                        </div>

                        <div class="input-group">
                            <select name="To" class="selectpicker" data-live-search="true"
                                    ng-model="ctrl.newJourney.to">
                                <option disabled selected value>To</option>
                                <option ng-repeat="airport in ctrlAirport.airports | orderBy : 'Names.en'"
                                        ng-value="airport.AirportCode">
                                    {{airport.Names.en}}
                                </option>
                            </select>
                            <span class="input-group-addon"><i class="material-icons">room</i></span>
                        </div>
                        <div class="input-group">
                            <input type="text" id="datepicker" class="datepicker" placeholder="Date" aria-label="Date"
                                   ng-model="ctrl.newJourney.date">
                            <span class="input-group-addon"><i class="material-icons">today</i></span>
                        </div>
                        <div class="input-group" id="daterange">
                            <input type="text" id="daterangepicker" class="datepicker" placeholder="Exact date"
                                   aria-label="Exact-date" ng-model="ctrl.newJourney.toDate">
                            <span class="input-group-addon"><i class="material-icons">today</i></span>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <input type="checkbox" aria-label="Direct" name="direct"
                                       ng-checked="ctrl.newJourney.direct"
                                       ng-model="ctrl.newJourney.direct"
                                >
                             </span>
                            <span class="input-group-text input-group-addon">Direct</span>
                        </div>
                        <button class="btn btn-outline-inverse" type="submit">Search</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%@ include file="FlightList.jsp" %>

    <div class="container element col-md-10">
        <div class="row">
            <div class="card col-md-4">
                <img class="card-img-top"
                     src="http://www.tapeciarnia.pl/tapety/normalne/tapeta-oswietlone-gdanskie-kamienice-odbijaja-sie-w-rzece-noca.jpg"
                     alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Place of the day</h5>
                    <p class="card-text">Gdansk</p>
                    <a href="https://pl.wikipedia.org/wiki/Gda%C5%84sk" class="btn btn-primary">Read more</a>
                </div>
            </div>

            <div class="col-md-2"></div>

            <div class="card col-md-5">
                <div class="card-header">
                    Best prices
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">first</li>
                    <li class="list-group-item">second</li>
                    <li class="list-group-item"></li>
                </ul>
            </div>
        </div>
    </div>
</main>


<script type="text/javascript">
    function journey() {
        if (document.getElementById('one-way').checked) {
            document.getElementById('daterange').style.visibility = 'hidden';
        } else {
            document.getElementById('daterange').style.visibility = 'visible';
        }
    }
</script>

<script src="/static/js/app.js"></script>
<script src="/static/js/services/user_service.js"></script>
<script src="/static/js/controllers/user_controller.js"></script>
<script src="/static/js/services/airport_service.js"></script>
<script src="/static/js/controllers/airport_controller.js"></script>
<script src="/static/js/services/journey_service.js"></script>
<script src="/static/js/controllers/journey_controller.js"></script>
<script src="/static/js/controllers/session_controller.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
        integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
        integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
        crossorigin="anonymous"></script>
</body>
<%@ include file="footer.jsp" %>
</html>
