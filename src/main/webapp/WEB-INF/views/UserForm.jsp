<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html>
<head>
    <title>FlightScanner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-cookies.js"></script>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">

    <script src="http://code.jquery.com/jquery.min.js"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">--%>
    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">--%>
    <%--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>--%>


    <style>
        .vertical-center {
            min-height: 100%;
            min-height: 100vh;

            display: flex;
            align-items: center;
            align-content: center;
        }

        .help-block {
            color: cadetblue;
            size: 10;
        }

        .with-errors {
            color: #c9001a;
            size: 10;
        }
    </style>

</head>

<body ng-app="flightapp">
<%@ include file="navbar.jsp" %>

<div class="jumbotron vertical-center">
    <div class="row container" ng-controller="UserController as ctrl">
        <div class="col-md-2"></div>
        <div col-md-4 col-md-offset-2>
            <form class="form-login" method="POST" action="/thickview/login">
                <h2 class="form-login-heading">Log in</h2>
                <div class="form-control-feedback">
                        <span class="text-danger">
                           <c:if test="${notification != ''}">
                               <i class="fa fa-close">${notification}</i>
                           </c:if>
                        </span>
                </div>
                <div class="form-group has-danger">
                    <label for="inputLogin" class="control-label">Login</label>
                    <input type="text" class="form-control" name="login" id="inputLogin" placeholder="Login" required>
                    <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                    <label for="inputPassword" class="control-label">Password</label>
                    <input type="password" data-minlength="8" name="password" class="form-control" id="inputPassword"
                           placeholder="Password" required>
                </div>

                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="form-check-input" name="remember">Remember me</label>
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Log in</button>
                </div>
            </form>
        </div>

        <div class="col-md-3"></div>

        <div class="col-md-4 col-md-offset-3">

            <form ng-submit="ctrl.sendNewUser()" name="form-register" data-toggle="validator" role="form">
                <h2 class="form-login-heading">Register</h2>
                <input type="hidden" ng-model="ctrl.newUser.id"/>

                <div class="form-control-feedback">
                        <span class="text-danger">
                               <i class="fa fa-close">{{ctrl.newUserMessage}}</i>
                        </span>
                </div>
                <div class="form-group">
                    <label for="login" class="control-label">Login</label>
                    <input type="text" ng-model="ctrl.newUser.login" class="form-control" id="login" placeholder="Login"
                           data-error="The login provided is not available." required>
                    <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                    <label for="email" class="control-label">Email</label>
                    <input type="email" ng-model="ctrl.newUser.userData.email" class="form-control" id="email"
                           placeholder="Email"
                           data-error="That email address is invalid" required>
                    <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                    <label for="password" class="control-label">Password</label>
                    <div class="form-group">
                        <input type="password" ng-model="ctrl.newUser.passwordHash" data-minlength="8"
                               class="form-control"
                               id="password"
                               placeholder="Password" required>
                        <span class="help-block">Minimum of 8 characters</span>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="passwordConfirm"
                               data-match="#password" data-match-error="These don't match"
                               placeholder="Confirm password" required>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="form-check-input" data-error="You have to accept the terms"
                                   value="accept-terms" required>I have read and accepted FlightScanner <a href="">Terms
                            & Conditions</a>
                        </label>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" value="Register" class="btn btn-primary" ng-disabled="form-register.$invalid"/>
                </div>
            </form>
        </div>
    </div>
</div>

<%@ include file="footer.jsp" %>

<script src="/static/js/app.js"></script>
<script src="/static/js/services/user_service.js"></script>
<script src="/static/js/controllers/user_controller.js"></script>
<script src="/static/js/controllers/session_controller.js"></script>

</body>
</html>
