'use strict';

angular.module('flightapp').controller('JourneyController', ['$scope', '$window', '$cookies','JourneyService',
    function ($scope, $window, $cookies ,JourneyService) {
        var self = this;
        self.schedules = null;
        self.schedule = null;
        self.newJourney = {
            "direct": false
        };
        self.flightInfo = null;

        console.log('Controller JourneyController created.');
        self.fetchJourneyWithIdMethod = fetchJourneyWithId;
        self.findJourney = findJourney;
        self.saveJourney = saveJourney;
        self.deleteJourney = deleteJourney;
        self.buyJourney = buyJourney;


        function fetchJourneyWithId(scheduleId) {
            console.log('Fetch schedule with id:' + scheduleId);
            JourneyService.fetchJourneyWithId(journeyId).then(
                function (response) {
                    console.log(response);
                    self.schedule = response.data.result.resultObject;
                },
                function (result) {
                    console.log(result);
                }
            );
        }

        function findJourney() {
            console.log('Sending findJourney request.');
            JourneyService.findJourney(self.newJourney).then(
                function (response) {
                    console.log("Journey finded.");
                    console.log(response);
                    self.schedules = response.data.result.resultObject;
                    console.log(self.schedules);
                },
                function (result) {
                    console.log("Unable to search journey.");
                    self.flightInfo ="No flight matched the criteria.";
                    console.log(result);
                }
            );
        }

        function saveJourney(scheduleId, userId) {
            console.log('Sending save journey request.');
            JourneyService.saveJourney(scheduleId, userId).then(
                function (response) {
                    console.log("Journey added.");
                    alert('Journey saved!');
                },
                function (result) {
                    console.log("Unable to save journey.");
                    console.log(result);
                }
            );
        }

        function deleteJourney(scheduleId, userId) {
            console.log('Sending delete journey request.');
            console.log('UserID : ' + userId);
            JourneyService.deleteJourney(scheduleId, userId).then(
                function (response) {
                    $window.location.reload();
                    console.log("Journey deleted.");
                    alert('Journey unsubscribed!');
                },
                function (result) {
                    console.log("Unable to delete journey.");
                    console.log(result);
                }
            );
        }

        function buyJourney(scheduleId, user) {
            console.log('Sending buy journey request.');
            JourneyService.buyJourney(scheduleId, user).then(
                function (response) {
                    console.log("Journey added to bought.");
                    $window.location.href = "https://www.lufthansa.com/";
                },
                function (result) {
                    console.log("Unable to buy journey.");
                    console.log(result);
                }
            );
        }

        $scope.onLoadFunction = function (id) {
            console.log('fuwd:' + id);
            fetchJourneyWithId(id);
        }

    }]);