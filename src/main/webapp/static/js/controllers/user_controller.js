'use strict';

angular.module('flightapp').controller('UserController', ['$scope', '$window', '$cookies', 'UserService',
    function ($scope, $window, $cookies, UserService) {
        var self = this;
        self.users = [];
        self.user = {
            "id": null,
            "login": "loading",
            "userData": {
                "id": null,
                "firstName": null,
                "lastName": null,
                "email": null,
                "address": {
                    "street": null,
                    "houseNumber": null,
                    "city": null,
                    "zipCode": null,
                    "country": null,
                }
            },
            "savedFlights": null,
            "flights": null
        };
        self.newUser = {
            "id": null,
            "login": "",
            "passwordHash": "",
            "userData": {
                "id": null,
                "firstName": null,
                "lastName": null,
                "email": null,
                "address": {
                    "street": null,
                    "houseNumber": null,
                    "city": null,
                    "zipCode": null,
                    "country": null,
                }
            }
        };
        self.userData = {
            "id": null,
            "firstName": null,
            "lastName": null,
            "email": null,
            "address": {
                "street": null,
                "houseNumber": null,
                "city": null,
                "zipCode": null,
                "country": null,
            },
            "flights": null,
            "savedFlights": null
        };
        self.newUserMessage = null;

        console.log('Controller created.');
        self.fetchAllUsersMethod = fetchAllUsers;
        self.fetchUserWithIdMethod = fetchUserWithId;
        self.sendNewUser = sendNewUser;
        self.updateUser = updateUser;

        fetchAllUsers();

        function fetchAllUsers() {
            UserService.fetchAllUsers().then(
                function (response) {
                    console.log(response);
                    self.users = response.data;
                },
                function (result) {
                    console.log(result);
                }
            );
        }

        function fetchUserWithId(userId) {
            console.log('Fetch user with id:' + userId);
            UserService.fetchUserWithId(userId).then(
                function (response) {
                    console.log(response);
                    self.user = response.data.result.resultObject;
                },
                function (result) {
                    console.log(result);
                }
            );
        }

        function sendNewUser() {
            self.newUserMessage = null;
            console.log('Sending new user request. Login:' + self.newUser.login);
            UserService.registerUser(self.newUser).then(
                function (response) {
                        console.log("User registered.");
                        $window.location.href = '/thickview/register';
                },
                function (result) {
                    console.log("Unable to register user.");
                    self.newUserMessage = 'User with that login already exist';
                    console.log(result);
                }
            );
        }

        function updateUser() {
            console.log('Updating user.');
            UserService.updateUser(self.user.id, self.userData).then(
                function (response) {
                    console.log("User updated.");
                },
                function (result) {
                    console.log("Unable to update user.");
                    console.log(result);
                }
            );
        }

        $scope.onLoadFunction = function () {
            var id = $cookies.get('userId');
            console.log('fuwd:' + id);
            fetchUserWithId(id);
        }

    }]);