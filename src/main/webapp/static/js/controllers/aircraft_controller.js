'use strict';

angular.module('flightapp').controller('AircraftController', ['$scope', '$window', '$cookies', 'AircraftService',
    function ($scope, $window, $cookies, AircraftService) {
        var self = this;
        self.aircrafts = [];
        self.aircraft = {
            "name": null,
            "position": {
                "longitude": null,
                "latitude": null
            },
            "onGround": null
        };

        console.log('Controller created.');
        self.fetchAllAircrafts = fetchAllAircrafts;
        self.fetchAircraftWithName = fetchAircraftWithName;

        fetchAllAircrafts();

        function fetchAllAircrafts() {
            AircraftService.fetchAllAircrafts().then(
                function (response) {
                    console.log(response);
                    self.aircrafts = response.data;
                },
                function (result) {
                    console.log(result);
                }
            );
        }

        function fetchAircraftWithName(name) {
            console.log('Fetch aircraft with name:' + name);
            AircraftService.fetchAircraftWithName(name).then(
                function (response) {
                    console.log(response);
                    self.aircraft = response.data.result.resultObject;
                },
                function (result) {
                    console.log(result);
                }
            );
        }

    }]);