'use strict';
angular.module('flightapp').controller('SessionController', ['$scope', '$window', '$cookies',
    function ($scope, $window, $cookies) {
        var self = this;
        self.loggedIn = false;
        self.userId = null;

        console.log('Session controller created.');

        $scope.isLoggedIn = function () {
            var sessionCookieContent = $cookies.get('session_cookie');
            if (sessionCookieContent === undefined) {
                $window.location.href = "/thickview/register"
            }
        }

        $scope.currentUser = function () {
            self.userId = $cookies.get('userId');
        }

        $scope.login = function (sessionId, userId) {
            console.log('Logging in with sessionId = ' + sessionId + ' and userId = ' + userId + ' (1 hour).');
            var date = new Date();
            date = new Date(date.getTime() + 3600000);
            console.log('Expiration date:' + date.toGMTString());
            $cookies.put('session_cookie', sessionId, {expires: '' + date.toGMTString()});
            $cookies.put('userId', userId, {expires: '' + date.toGMTString()});
            self.loggedIn=true;
            self.userId = userId;
            $window.location.href = "/thickview/homepage";
        }

        $scope.loginWithRemember = function (sessionId, userId) {
            console.log('Logging in with sessionId = ' + sessionId + ' and userId = ' + userId + ' (1 year).');
            var date = new Date();
            date = new Date(date.getTime() + (3600000*24*365));
            console.log('Expiration date:' + date.toGMTString());
            $cookies.put('session_cookie', sessionId, {expires: '' + date.toGMTString()});
            $cookies.put('userId', userId, {expires: '' + date.toGMTString()});
            self.loggedIn=true;
            self.userId = userId;
            $window.location.href = "/thickview/homepage";
        }

        $scope.logout = function(sessionId) {
            console.log('Logging out with sessionId = ' + sessionId);
            $cookies.remove('session_cookie');
            $cookies.remove('userId');
            self.loggedIn=false;
            $window.location.href = "/thickview/register";
        }

        $scope.isLoggedInForNavbar = function () {
            var sessionCookieContent = $cookies.get('session_cookie');
            if (sessionCookieContent === undefined) {
                self.loggedIn=false;
            } else{
                self.loggedIn=true;
            }
        }


    }]);