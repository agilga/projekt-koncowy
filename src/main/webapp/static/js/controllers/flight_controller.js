'use strict';

angular.module('flightapp').controller('FlightController', ['$scope', 'FlightService',
    function ($scope, FlightService) {
        var self = this;
        self.flights = [];

        console.log('Controller created.');
        self.fetchAllFlights = fetchAllFlights;

        fetchAllFlights();

        function fetchAllFlights() {
            console.log('fetchinggggg flights');
            FlightService.fetchAllFlights().then(
                function (response) {
                    console.log(response);
                    self.flights = response.data;
                },
                function (result) {
                    console.log(result);
                }
            );
        }


    }]);