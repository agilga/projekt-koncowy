'use strict';

angular.module('flightapp').controller('AirportController', ['$scope', 'AirportService',
    function ($scope, AirportService) {
        var self = this;
        self.airports = [];
        self.airport = null;

        console.log('Controller created.');
        self.fetchAllAirportsMethod = fetchAllAirports;
        self.fetchAirportWithIdMethod = fetchAirportWithId;

        fetchAllAirports();

        function fetchAllAirports() {
            console.log('fetching airporrrts');
            AirportService.fetchAllAirports().then(
                function (response) {
                    console.log(response);
                    self.airports = response.data;
                },
                function (result) {
                    console.log(result);
                }
            );
        }

        function fetchAirportWithId(airportId) {
            console.log('Fetch airport with id:' + airportId);
            AirportService.fetchAirportWithId(airportId).then(
                function (response) {
                    console.log(response);
                    self.airport = response.data.result.resultObject;
                },
                function (result) {
                    console.log(result);
                }
            );
        }

        $scope.onLoadFunction = function (id) {
            console.log('fuwd:' + id);
            fetchAirportWithId(id);
        }

    }]);
