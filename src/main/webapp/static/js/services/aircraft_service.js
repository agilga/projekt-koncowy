'use strict';

angular.module('flightapp').factory('AircraftService', ['$http', '$q',
    function ($http, $q) {

        var RESTAPIURL = 'http://localhost:8080/';
        var factory = {
            fetchAllAircrafts: fetchAllAircrafts,
            fetchAircraftWithName: fetchAircraftWithName,
        };
        console.log('Factory created.');
        return factory;

        function fetchAllAircrafts() {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'aircrafts').then(
                function (result) {
                    deferred.resolve(result);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function fetchAircraftWithName(name) {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'aircraft/' + name).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

    }]);