'use strict';

angular.module('flightapp').factory('JourneyService', ['$http', '$q',
    function ($http, $q) {

        var RESTAPIURL = 'http://localhost:8080/';
        var factory = {
            fetchJourneyWithId: fetchJourneyWithId,
            findJourney: findJourney,
            saveJourney: saveJourney,
            deleteJourney: deleteJourney,
            buyJourney: buyJourney
        };
        console.log('Factory created.');
        return factory;

        function fetchJourneyWithId(id) {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'schedule/' + id).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function findJourney(newJourney) {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'LH/schedules/origin/'+newJourney.from+'/destination/'+newJourney.to+'/data/'+ newJourney.date+'?direct=' + newJourney.direct ).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function saveJourney(scheduleId, userId) {
            var deferred = $q.defer();
            $http.put(RESTAPIURL + 'rest/user/' + userId+'/save-schedule/' + scheduleId + '/').then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function deleteJourney(scheduleId, userId) {
            var deferred = $q.defer();
            $http.put(RESTAPIURL + 'rest/user/' + userId +'/usubscribe-schedule/' + scheduleId + '/').then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function buyJourney(scheduleId, userId) {
            var deferred = $q.defer();
            $http.put(RESTAPIURL + 'rest/user/' + userId +'/buy-schedule/' + scheduleId).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

    }]);