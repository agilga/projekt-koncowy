'use strict';

angular.module('flightapp').factory('FlightService', ['$http', '$q',
    function ($http, $q) {

        var RESTAPIURL = 'http://localhost:8080/rest/';

        var factory = {
            fetchAllFlights: fetchAllFlights
        };
        console.log('Factory created.');
        return factory;


        function fetchAllFlights() {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'getFlights').then(
                function (data) {
                    // sukces
                    deferred.resolve(data);
                }, function (result) {
                    // nie udalo sie
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

    }]);