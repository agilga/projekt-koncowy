'use strict';

angular.module('flightapp').factory('AirportService', ['$http', '$q',
    function ($http, $q) {

        var RESTAPIURL = 'http://localhost:8080/rest/';
        var factory = {
            fetchAllAirports: fetchAllAirports,
            fetchAirportWithId: fetchAirportWithId
        };
        console.log('Factory created.');
        return factory;

        function fetchAllAirports() {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'getAirports').then(
                function (result) {
                    deferred.resolve(result);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function fetchAirportWithId(id) {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'airport/' + id).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

    }]);